import { IEthereuemState, ITransaction, ITransactionLog } from "./types";

const state: IEthereuemState = {
  web3: undefined,
  coinbase: "",
  defaultAccount: "",
  networkId: undefined,
  isListening: false,
  contract: undefined,
  balance: "0",
  isContract: false,
  privateKey: "",
  decimals: undefined,
  abiReader: undefined,
  contractName: "",
  transactions: new Array<ITransaction>(),
  transactionLogs: new Map<string, ITransactionLog>()
};

export default state;
