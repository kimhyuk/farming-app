import Vue from "vue";
import * as SocketIO from "nativescript-socket.io";
SocketIO.enableDebug();

Vue.use((Vue, options) => {
	const io = SocketIO.connect("http://18.220.166.214:1337");

	Vue.prototype.$io = io;
});
