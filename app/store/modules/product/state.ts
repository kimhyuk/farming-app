import { IProduct, IProductState, Product } from "./types";

const state: IProductState = {
  products: new Array<IProduct>(),
  modifyProduct: new Product()
};

export default state;
