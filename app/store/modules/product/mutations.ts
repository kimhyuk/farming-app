import { MUTATIONS } from "./constants";
import { MutationTree } from "vuex";
import { IProductState, IProduct, Product } from "./types";
import _ from "lodash";

// mutations
const mutations: MutationTree<IProductState> = {
  [MUTATIONS.ADD_PRODUCT](state, products: Product[]) {
    state.products = _.concat(
      state.products,
      _.map(products, p => {
        return p instanceof Product ? p : new Product(p);
      })
    );
  },
  [MUTATIONS.DELETE_PRODUCT](state, idx) {
    state.products.slice(idx, 1);
  },
  [MUTATIONS.INIT_PRODUCTS](state, products: Product[]) {
    state.products = _.map(products, p => {
      return p instanceof Product ? p : new Product(p);
    });
  }
};

export default mutations;
