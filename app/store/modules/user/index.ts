import { IRootState } from "@/store/types";
import { Module } from "vuex";
import UserState from "./state";
import actions from "./actions";
import mutations from "./mutations";
import { IUserState } from "./types";
import constants from "./constants";

const user: Module<UserState, IRootState> = {
  namespaced: true,
  state: new UserState(),
  actions,
  mutations
};

export { IUserState, constants };

export default user;
