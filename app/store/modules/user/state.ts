import { IUserState } from "./types";
import jwtDecode from "jwt-decode";
import isElectron from "is-electron";

const electronStorage = isElectron()
  ? require("electron-json-storage")
  : undefined;

class UserState implements IUserState {
  _token: string = "";
  refreshToken = null;
  email = "";
  username = "";
  _id = "";
  password = "";
  address = "";
  privateKey = "";

  constructor() {
    if (electronStorage) {
      electronStorage.get("token", (err: any, token: any) => {
        this._token = token;
      });
    } else {
      this._token = <string>localStorage.getItem("token");
    }
  }

  get token() {
    return this._token;
  }
  set token(token) {
    if (electronStorage) {
      electronStorage.set("token", token, (err: any) => {});
    } else {
      localStorage.setItem("token", token);
    }
    this._token = token;
  }

  deleteToken() {
    if (electronStorage) {
      electronStorage.remove("token", (err: any) => {
        if (err) throw err;
      });
    } else {
      localStorage.removeItem("token");
    }
  }
}

export default UserState;
