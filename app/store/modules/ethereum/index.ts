import { Module } from "vuex";
import { IEthereuemState } from "./types";
import { IRootState } from "@/store/types";
import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";
import state from "./state";
import constants from "./constants";

const ether: Module<IEthereuemState, IRootState> = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};

export { constants, IEthereuemState };
export default ether;
