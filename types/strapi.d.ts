import Vue from "vue";
import Strapi from "strapi-sdk-javascript";

declare module "@nuxt/vue-app" {
	interface Context {
		$strapi: Strapi;
		$apiUrl: string;
		$baseUrl: string;
		$eth: any;
	}
}

declare module "vue/types/vue" {
	interface Vue {
		$strapi: Strapi;
		$apiUrl: string;
		$baseUrl: string;
		$eth: any;
	}
}
