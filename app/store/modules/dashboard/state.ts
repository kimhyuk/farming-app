import { IDashboardState } from "./types";

const state: IDashboardState = {
  drawerVisible: false,
  snackbar: {
    message: undefined,
    visible: false
  },
  dialogs: []
};

export default state;
