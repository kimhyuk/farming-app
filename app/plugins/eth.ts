import Vue from "vue";
const Eth = require("ethjs");

Vue.use((Vue, options) => {
	const eth = new Eth(new Eth.HttpProvider("http://18.220.166.214:8545", 1000));
	Vue.prototype.$eth = eth;
});
