import strapi from "@/util/StrapiSdk";
import { API_URL } from "@/constants";
import Vue from "vue";

Vue.use((Vue, options) => {
	Vue.prototype.$strapi = strapi;
	Vue.prototype.$apiUrl = API_URL;
});
