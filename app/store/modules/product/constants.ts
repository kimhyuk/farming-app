const COMMON = {
  ADD_PRODUCT: "ADD_PRODUCT",
  DELETE_PRODUCT: "DELETE_PRODUCT",
  INIT_PRODUCTS: "INIT_PRODUCTS"
};

export const ACTIONS = {
  ...COMMON
};

export const MUTATIONS = {
  ...COMMON
};

export const GETTERS = {};

export const NAME = "product";

export default {};
