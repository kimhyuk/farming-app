import _ from "lodash";

export enum STATEMUTABILITY {
	nonpayable = "nonpayable",
	view = "view"
}

export enum TYPE {
	function = "function",
	event = "event",
	constructor = "constructor"
}

export interface IAbiInput {
	indexed?: boolean;
	name: string;
	type: string;
}

export interface IAbiOutput {
	name: string;
	type: string;
}

export class Abi {
	private _constant: boolean;
	private _name: string;
	private _payable: boolean;
	private _stateMutability: STATEMUTABILITY;
	private _type: TYPE;
	private _inputs: [IAbiInput];
	private _outputs: [IAbiOutput];

	constructor(abi: Abi) {
		this._constant = abi._constant;
		this._name = abi._name;
		this._payable = abi._payable;
		this._stateMutability = abi._stateMutability;
		this._type = abi._type;
		this._inputs = abi._inputs;
		this._outputs = abi._outputs;
	}

	get name(): string {
		return this._name;
	}

	get contant(): boolean {
		return this._constant;
	}

	get payable(): boolean {
		return this._payable;
	}

	get stateMutability(): STATEMUTABILITY {
		return this._stateMutability;
	}

	get type(): TYPE {
		return this._type;
	}

	get inputs(): [IAbiInput] {
		return this._inputs;
	}

	get outputs(): [IAbiOutput] {
		return this._outputs;
	}

	isFunction(): boolean {
		return this.type == TYPE.function;
	}
}

export class AbiReader {
	_abis: [Abi];

	constructor(abis: [Abi]) {
		this._abis = abis;
	}

	find(functionName: string): Abi | undefined {
		const index = _.findIndex<Abi>(this._abis, ["name", functionName]);

		return index == -1 ? undefined : this._abis[index];
	}

	sendTransaction(functionName: string) {}
}
