import { ActionTree } from "vuex";
import { ACTIONS, MUTATIONS } from "./constants";
import UserState from "./state";
import { IUserState } from "./types";
import { IRootState } from "@/store/types";
import jwtDecode from "jwt-decode";
import isElectron from "is-electron";
import AuthService, { ChangeInfoType } from "@/services/AuthService";
import _ from "lodash";
const privateToAccount = require("ethjs-account").privateToAccount;

const electronStorage = isElectron()
  ? require("electron-json-storage")
  : undefined;

const actions: ActionTree<UserState, IRootState> = {
  async [ACTIONS.LOGIN]({ commit, dispatch, state }, loginType) {
    try {
      let userInfo;
      if (loginType) {
        dispatch(ACTIONS.LOGOUT);
        const response = await AuthService.login(loginType);

        userInfo = response.user;
        commit(MUTATIONS.SET_TOKEN, response.jwt);
      } else {
        const result = await new Promise(async (resolve, reject) => {
          setTimeout(() => {
            resolve(state.token);
          }, 1000);
        });
        if (!result) return false;
        AuthService.setToken(state.token);
        userInfo = await AuthService.getMe();
      }
      commit(MUTATIONS.SET_USER, userInfo);

      return true;
    } catch (err) {
      return err;
    }
  },
  async [ACTIONS.SIGNUP]({ commit, dispatch, state }, signupType) {
    try {
      const { address } = privateToAccount(signupType.privateKey);

      const response = await AuthService.signup({
        ...signupType,
        address
      });

      const userInfo = response.user;
      commit(MUTATIONS.SET_TOKEN, response.jwt);
      AuthService.setToken(response.jwt);
      commit(MUTATIONS.SET_USER, userInfo);

      return true;
    } catch (err) {
      return err;
    }
  },

  async [ACTIONS.REGISTER_SELLER]({ commit }) {
    return await AuthService.registerSeller();
  },

  async [ACTIONS.LOGOUT]({ commit, dispatch, state }) {
    try {
      AuthService.clearToken();

      commit(MUTATIONS.CLEAR_TOKEN);
      commit(MUTATIONS.CLEAR_USER);
    } catch (err) {}
  },

  async [ACTIONS.CHANGE_INFO]({ commit, dispatch, state }) {
    try {
      const changeInfoType = <ChangeInfoType>(
        _.pick(state, ["address", "pricateKey"])
      );
      const changeInfo = await AuthService.changeInfo(changeInfoType);

      console.log(changeInfo);
      return true;
    } catch (err) {
      return err;
    }
  },

  async [ACTIONS.SET_TOKEN]({ commit, dispatch }, token) {
    return new Promise((resolve, reject) => {
      try {
        if (electronStorage) {
          electronStorage.set("token", token, (err: any) => {
            if (err) throw err;

            return resolve(true);
          });
        } else {
          localStorage.setItem("token", token);
          dispatch(ACTIONS.SET_USER, token);

          return resolve(true);
        }
      } catch (err) {
        return reject(err);
      }
    });
  },
  async [ACTIONS.DELETE_TOKEN]({ commit, dispatch }) {
    return new Promise((resolve, reject) => {
      try {
        if (electronStorage) {
          electronStorage.remove("token", (err: any) => {
            if (err) throw err;

            return resolve(true);
          });
        } else {
          localStorage.removeItem("token");
          return resolve(true);
        }
      } catch (err) {
        reject(err);
      }
    });
  },
  async [ACTIONS.GET_TOKEN]({ commit, dispatch }) {
    return new Promise((resolve, reject) => {
      try {
        if (electronStorage) {
          electronStorage.get("token", (err: any, data: any) => {
            resolve(data);
          });
        } else {
          const token = <string>localStorage.getItem("token");
          dispatch(ACTIONS.SET_USER, token);

          return resolve(token);
        }
      } catch (err) {
        reject(err);
      }
    });
  },

  async [ACTIONS.SET_USER]({ commit, dispatch }, token) {
    var decoded = jwtDecode(token);
    commit(MUTATIONS.SET_USER, decoded);
  },

  [ACTIONS.SET_EMAIL]({ commit, dispatch }, email) {
    commit(MUTATIONS.SET_EMAIL, email);
  },

  [ACTIONS.SET_PRIVATEKEY]({ commit, dispatch }, privateKey) {
    const { address } = privateToAccount(privateKey);

    commit(MUTATIONS.SET_PRIVATEKEY, privateKey);
    commit(MUTATIONS.SET_ADDRESS, address);
  }
};

export default actions;
