import { Component, Vue } from "vue-property-decorator";
import { Getter, Action } from "vuex-class";
import getWeb3 from "@/util/getEthjs";
import { constants as ETHEREUM } from "@/store/modules/ethereum";

@Component
export default class WalletMixin extends Vue {
	@Action(ETHEREUM.ACTIONS.INIT, { namespace: ETHEREUM.NAME })
	etherInit!: Function;

	async SetEther(key: string) {
		try {
			console.log(key);
			let web3 = await getWeb3({
				type: "privateKey",
				privateKey: key
			});

			this.etherInit(web3);

			return Promise.resolve(true);
		} catch (err) {
			return Promise.reject(err);
		}
	}
}
