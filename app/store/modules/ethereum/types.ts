import { AbiReader } from "@util/AbiReader";

export interface ITransactionLog {
	hash: string;
	nonce: number;
	blockHash: string;
	blockNumber: number;
	transactionIndex: number;
	from: string;
	to: string;
	value: string;
	gas: number;
	gasPrice: string;
	input: string;
}

export enum TRANSACTION_STATUS {
	PENDING = "pending",
	SUCCESS = "success"
}

export interface ITransaction {
	hash: string;
	status: TRANSACTION_STATUS;
}
export interface IEthereuemState {
	web3?: any;
	defaultAccount?: string;
	coinbase?: string;
	networkId?: number;
	isListening?: boolean;
	contract?: any;
	balance?: string;
	isContract?: boolean;
	privateKey?: string;
	decimals?: number;
	abiReader?: AbiReader;
	contractName?: string;
	transactions: Array<ITransaction>;
	transactionLogs: Map<string, ITransactionLog>;
}
