import { ACTIONS } from "./constants";
import { ActionTree } from "vuex";
import { IRootState } from "@/store/types";
import { IDashboardState } from "./types";

const actions: ActionTree<IDashboardState, IRootState> = {
  [ACTIONS.DRAWER_VISIBLE_CHANGE]({ commit }) {
    commit(ACTIONS.DRAWER_VISIBLE_CHANGE);
  },
  [ACTIONS.SET_ON_SNACKBAR]({ commit }, options) {
    commit(ACTIONS.SET_ON_SNACKBAR, options);
  }
};

export default actions;
