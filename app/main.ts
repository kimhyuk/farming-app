import Vue from "nativescript-vue";

require("nativescript-localstorage");

require("nativescript-nodeify");

require("node-libs-browser");

import Login from "@/components/Login.vue";
import "./plugins";

import VueDevtools from "nativescript-vue-devtools";
import store from "./store";

(<any>Vue).registerElement(
	"BarcodeScanner",
	() => require("nativescript-barcodescanner").BarcodeScannerView
);

if (TNS_ENV !== "production") {
	Vue.use(VueDevtools);
}

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = TNS_ENV === "production";

Vue.registerElement(
	"RadSideDrawer",
	() => require("nativescript-ui-sidedrawer").RadSideDrawer
);

new Vue({
	store,
	render: h => h("frame", [h(Login)])
}).$start();
