const COMMON = {
  DRAWER_VISIBLE_CHANGE: "DRAWER_VISIBLE_CHANGE",
  SET_ON_SNACKBAR: "SET_ON_SNACKBAR"
};

export const ACTIONS = {
  ...COMMON
};

export const MUTATIONS = {
  ...COMMON
};

export const NAME = "dashboard";

export default {
  ACTIONS,
  MUTATIONS,
  NAME
};
