import Vue from "vue";

declare module "@nuxt/vue-app" {
  interface Context {
    $eventBus: Vue;
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $eventBus: Vue;
  }
}
