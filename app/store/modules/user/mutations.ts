import { MUTATIONS } from "./constants";
import { MutationTree } from "vuex";
import UserState from "./state";
import electronStorage from "electron-json-storage";

// mutations
const mutations: MutationTree<UserState> = {
  [MUTATIONS.SET_USER](state, user) {
    state._id = user._id;
    state.email = user.email;
    state.username = user.username;
    state.privateKey = user.privateKey;
  },
  [MUTATIONS.CLEAR_USER](state) {
    state._id = "";
    state.email = "";
    state.username = "";
    state.privateKey = "";
    state.address = "";
  },
  [MUTATIONS.SET_EMAIL](state, email) {
    state.email = email;
  },
  [MUTATIONS.SET_ADDRESS](state, address) {
    state.address = address;
  },
  [MUTATIONS.SET_USERNAME](state, username) {
    state.username = username;
  },
  [MUTATIONS.SET_PRIVATEKEY](state, privateKey) {
    state.privateKey = privateKey;
  },
  [MUTATIONS.SET_ADDRESS](state, address) {
    state.address = address;
  },
  [MUTATIONS.SET_TOKEN](state, jwt) {
    state.token = jwt;
  },
  [MUTATIONS.CLEAR_TOKEN](state) {
    state.token = "";
    state.deleteToken();
  }
};

export default mutations;
