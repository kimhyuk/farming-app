import { GetterTree } from "vuex";
import { IEthereuemState } from "./types";
import { IRootState } from "@/store/types";
import { GETTERS } from "./constants";

// getters
const getters: GetterTree<IEthereuemState, IRootState> = {
  [GETTERS.GET_SYMBOL_BALANCE]: state => {
    const balance = state.balance ? state.balance : "0";
    if (!state.isContract) {
      return String(parseInt(balance) / 10 ** 18) + " eth";
    } else {
      return (
        String(parseInt(balance) / <number>state.decimals) +
        " " +
        state.contractName
      );
    }
  }
};

export default getters;
