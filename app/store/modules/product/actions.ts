import { ACTIONS, MUTATIONS } from "./constants";
import { ActionTree } from "vuex";
import { IRootState } from "@/store/types";
import { IProduct, IProductState } from "./types";

const actions: ActionTree<IProductState, IRootState> = {
  [ACTIONS.ADD_PRODUCT]({ state, commit }, products) {
    commit(MUTATIONS.ADD_PRODUCT, products);
  },
  [ACTIONS.DELETE_PRODUCT]({ state, commit }, idx) {
    commit(MUTATIONS.DELETE_PRODUCT, idx);
  },
  [ACTIONS.INIT_PRODUCTS]({ state, commit }, products) {
    commit(MUTATIONS.INIT_PRODUCTS, products);
  }
};

export default actions;
