import { ACTIONS, MUTATIONS } from "./constants";
import { ActionTree } from "vuex";
import { IRootState } from "@/store/types";
import { IEthereuemState } from "./types";
import isElectron from "is-electron";
import { TYPE, STATEMUTABILITY } from "@/util/AbiReader";
import utils from "web3-utils";
import { Transaction as Tx } from "ethereumjs-tx";
import { Buffer } from "buffer";

const electronStorage = isElectron()
	? require("electron-json-storage")
	: undefined;

const actions: ActionTree<IEthereuemState, IRootState> = {
	[ACTIONS.CLEAR]({ commit }) {
		commit(MUTATIONS.CLEAR);
	},
	[ACTIONS.INIT]({ commit }, payload) {
		commit(MUTATIONS.INIT, payload);
	},
	[ACTIONS.SET_WEB3]({ commit }, payload) {
		commit(MUTATIONS.SET_WEB3, payload);
	},
	[ACTIONS.SET_STATUS]({ commit }, payload) {
		commit(MUTATIONS.SET_STATUS, payload);
	},
	[ACTIONS.SET_CONTRACT]({ commit }, payload) {
		commit(MUTATIONS.SET_CONTRACT, payload);
	},
	[ACTIONS.SET_IS_CONTRACT]({ commit }, payload) {
		commit(MUTATIONS.SET_IS_CONTRACT, payload);
	},

	[ACTIONS.SET_ABI_READER]({ commit }, payload) {
		commit(MUTATIONS.SET_ABI_READER, payload);
	},

	[ACTIONS.SET_CONTRACT_NAME]({ commit }, payload) {
		commit(MUTATIONS.SET_CONTRACT_NAME, payload);
	},
	async [ACTIONS.SET_PRIVATE_KEY](_, privateKey) {
		if (electronStorage) {
			electronStorage.set("privateKey", privateKey, (err: any) => {
				if (err) throw err;

				return Promise.resolve(true);
			});
		} else {
			localStorage.setItem("privateKey", privateKey);
			return Promise.resolve(true);
		}
	},
	async [ACTIONS.DELETE_PRIVATE_KEY](_) {
		if (electronStorage) {
			electronStorage.remove("privateKey", (err: any) => {
				if (err) throw err;

				return Promise.resolve(true);
			});
		} else {
			localStorage.removeItem("privateKey");
			return Promise.resolve(true);
		}
	},
	async [ACTIONS.GET_PRIVATE_KEY](_) {
		if (electronStorage) {
			return new Promise((resolve, reject) => {
				electronStorage.get("privateKey", (err: any, data: any) => {
					if (err) throw err;

					return resolve(typeof data === "object" ? "" : data);
				});
			});
		} else {
			const privateKey = localStorage.getItem("privateKey");
			return Promise.resolve(privateKey);
		}
	},

	async [ACTIONS.REFRESH_BALANCE]({ state, commit }) {
		try {
			const { web3, contract, defaultAccount } = state;

			let decimal = state.decimals;

			let balance: string | number = 0;

			if (!state.isContract) {
				if (!web3) throw new Error("web3 is undefined");
				if (!defaultAccount) throw new Error("No Address");

				balance = await web3.eth.getBalance(defaultAccount);
				console.log(balance);
				decimal = 18;
			} else {
				if (!contract) throw new Error("contract is undefined");

				balance = await contract.methods.balanceOf(state.defaultAccount).call();
				decimal = await contract.methods.decimals().call();
			}
			commit(MUTATIONS.SET_BALANCE, balance);
			commit(MUTATIONS.SET_DACIMALS, decimal);
			return Promise.resolve();
		} catch (err) {
			return Promise.reject(err);
		}
	},

	async [ACTIONS.SEND_TRANSACTION]({ state, commit }, { name, inputs }) {
		try {
			const { web3, contract, abiReader, defaultAccount } = state;

			if (!web3) throw new Error("web3 is undefined");
			if (!abiReader) throw new Error("abiReader is undefined");

			const abi = abiReader.find(name);

			if (!abi) throw new Error(`${name} is not function`);
			if (abi.inputs.length !== inputs.length)
				throw new Error("입력값이 부족합니다.");
			if (!contract) throw new Error("contract is undefined");

			if (abi.stateMutability === STATEMUTABILITY.view) {
				return contract.methods[name].apply(null, inputs).call();
			} else {
				return contract.methods[name].apply(null, inputs).send({
					from: defaultAccount
				});
			}
		} catch (err) {
			throw err;
		}
	},

	[ACTIONS.ADD_TRANSACTION]({ dispatch, commit, state }, { hash, status }) {
		commit(MUTATIONS.ADD_TRANSACTION, { hash, status });
	},
	async [ACTIONS.SEND_COIN]({ dispatch, commit, state }, payload) {
		try {
			const { web3, defaultAccount, contract, isContract, privateKey } = state;
			let decimal = state.decimals;

			if (isContract) {
				if (!contract) throw new Error("contract is undefined");
				const { transfer, deciamls } = contract.methods;

				if (!decimal) {
					decimal = await deciamls().call();
					commit(MUTATIONS.SET_DACIMALS, decimal);
				}

				const balance = Number(payload.value) * Math.pow(10, Number(decimal));
				const hash = await transfer(payload.to, String(balance)).send({
					from: defaultAccount
				});

				dispatch(ACTIONS.ADD_TRANSACTION, { hash });

				return Promise.resolve(hash);
			} else {
				if (!web3) throw new Error("web3 is undefined");

				//const gasPrice = await web3.eth.getGasPrice();

				const nonce = await web3.eth.getTransactionCount(defaultAccount);

				console.log(payload);
				console.log("transfer_1");
				const value = (() => {
					if (payload.unitType == "wei") {
						return Number(payload.value).toString(16);
					} else {
						return utils.toWei(String(payload.value), "ether");
					}
				})();

				console.log("transfer_2");

				console.log(value);

				var gasLimit = await web3.eth.estimateGas({
					to: payload.to,
					from: defaultAccount,
					value: value
				});

				console.log("transfer_3");

				var rawTx = {
					nonce: "0x" + nonce.toString(16),
					gasPrice: "0x" + Number(21000).toString(16),
					gasLimit: "0x" + Number(gasLimit).toString(16),
					to: payload.to,
					value: "0x" + value,
					data: "0x"
				};

				console.log("transfer_4");

				let tx = new Tx(rawTx);

				tx.sign(Buffer.from(privateKey.slice(2), "hex"));

				let serializedTx = tx.serialize();

				return web3.eth.sendRawTransaction("0x" + serializedTx.toString("hex"));

				// return new Promise((resolve, reject) => {
				// 	web3.eth.sendTransaction(
				// 		{
				// 			from: defaultAccount,
				// 			to: payload.to,
				// 			value: utils.toWei(String(payload.value), "ether"),
				// 			gasPrice: payload.gasPrice || gasPrice,
				// 			gas: payload.gas || gasLimit
				// 		},
				// 		(error: any, hash: string) => {
				// 			if (error) return reject(error);
				// 			dispatch(ACTIONS.ADD_TRANSACTION, { hash });
				// 			resolve(hash);
				// 		}
				// 	);
				// });
			}
		} catch (err) {
			return Promise.reject(err);
		}
	}
};

export default actions;
