import Vue from "vue";

declare module "@nuxt/vue-app" {
	interface Context {
		$io: SocketIOClient.Socket;
		$navigateTo: Function;
	}
}

declare module "vue/types/vue" {
	interface Vue {
		$navigateTo: Function;
		$io: SocketIOClient.Socket;
	}
}
