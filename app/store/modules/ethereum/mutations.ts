import { MUTATIONS } from "./constants";
import { MutationTree } from "vuex";
import { IEthereuemState } from "./types";
import { AbiReader } from "@/util/AbiReader";
import { TRANSACTION_STATUS } from "./types";

// mutations
const mutations: MutationTree<IEthereuemState> = {
	[MUTATIONS.CLEAR](state) {
		delete state.web3;
		delete state.coinbase;
		delete state.networkId;
		delete state.isListening;
		delete state.defaultAccount;
	},
	[MUTATIONS.INIT](state, payload: IEthereuemState) {
		state.web3 = payload.web3;
		state.coinbase = payload.coinbase;
		state.networkId = payload.networkId;
		state.isListening = payload.isListening;
		state.defaultAccount = payload.defaultAccount;
		state.privateKey = payload.privateKey;
	},

	[MUTATIONS.SET_WEB3](state, payload) {
		state.web3 = payload;
	},

	[MUTATIONS.SET_CONTRACT](state, payload) {
		state.contract = payload;
	},
	[MUTATIONS.SET_IS_CONTRACT](state, payload) {
		state.isContract = payload;
	},
	[MUTATIONS.SET_BALANCE](state, payload) {
		state.balance = payload;
	},
	[MUTATIONS.SET_DACIMALS](state, payload) {
		state.decimals = payload;
	},
	[MUTATIONS.SET_CONTRACT_NAME](state, payload) {
		state.contractName = payload;
	},
	[MUTATIONS.ADD_TRANSACTION](state, { hash, status }) {
		if (status === "undefined") {
			status = TRANSACTION_STATUS.PENDING;
		}
		state.transactions.push({ hash, status });
	},

	[MUTATIONS.SET_ABI_READER](state, payload) {
		state.abiReader = new AbiReader(payload);
	}
};

export default mutations;
