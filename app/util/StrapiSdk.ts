import Strapi from "strapi-sdk-javascript";
import { API_URL } from "@/constants";

export default new Strapi(API_URL, {
  localStorage: false,
  cookie: false
});
