export interface IProduct {
  _id: string;
  productName: string;
  image: string;
  price: number;
}

export interface IProductState {
  products: Product[];
  modifyProduct: IProduct;
}

export class Product implements IProduct {
  _id = "";
  productName = "";
  image = "";
  price = 0;

  constructor(product?: IProduct) {
    if (product) {
      this._id = product._id;
      this.productName = product.productName;
      this.image = product.image;
      this.price = product.price;
    }
  }
}
