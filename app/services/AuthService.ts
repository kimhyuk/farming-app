import strapi from "@/util/StrapiSdk";

export type LoginType = {
  identifier: string;
  password: string;
};

export type SignupType = {
  email: string;
  username: string;
  password: string;
  address: string | null;
  privateKey: string;
};

export type ChangeInfoType = {
  address: string;
  privateKey: string;
};

class AuthServiece {
  static setToken(token: string) {
    strapi.setToken(token);
  }

  static clearToken() {
    strapi.clearToken();
  }

  static async login(loginType: LoginType) {
    const { identifier, password } = loginType;
    return strapi.login(identifier, password);
  }

  static async signup(signupType: SignupType) {
    return strapi.request("POST", "/auth/local/register", {
      data: signupType
    });
  }

  static async getMe() {
    return strapi.request("GET", "/users/me");
  }

  static async changeInfo(changeInfoType: ChangeInfoType) {
    return strapi.request("PUT", "/users-permissions/user", {
      data: changeInfoType
    });
  }

  static async registerSeller() {
    return strapi.request("POST", "/users-permissions/seller/register");
  }
}

export default AuthServiece;
