export interface IUserState {
  token: string;
  refreshToken: string | null;
  _id: string | null;
  email: string | null;
  username: string | null;
  password: string | null;
  address: string | null;
  privateKey: string | null;
}
