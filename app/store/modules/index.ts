//import ethereum from "./ethereum";
import dashboard from "./dashboard";
import user from "./user";
import product from "./product";
import ethereum from "./ethereum";

export default {
	dashboard,
	user,
	product,
	ethereum
};
