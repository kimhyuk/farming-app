const Eth = require("ethjs");
const privateToAccount = require("ethjs-account").privateToAccount;
import axios from "axios";

type getWeiPrams = {
	type: string;
	privateKey?: string;
};

async function getApp(eth: any, privateKey: string) {
	const [isListening, coinbase] = await Promise.all([
		eth.net_listening(),
		eth.coinbase()
	]);

	const account = privateToAccount(privateKey);

	//let address = web3.eth.defaultAccount;
	const app = {
		web3: {
			eth
		},
		isListening,
		networkId: null,
		coinbase,
		defaultAccount: account.address,
		privateKey: privateKey
	};
	return Promise.resolve(app);
}

async function loadPrivateKeyWallet(privateKey: string) {
	const eth = new Eth({
		sendAsync: async (payload, callback) => {
			let error = null;
			let response = null;
			try {
				response = await axios.post("http://18.220.166.214:8545", payload, {
					headers: {
						"Content-Type": "application/json",
						"Access-Control-Allow-Origin": "*"
					}
				});
			} catch (err) {
				error = err;
			} finally {
				callback(error, response.data);
			}
		}
	});
	console.log(await eth.net_listening());
	return getApp(eth, privateKey);
}

export default async (params: getWeiPrams) => {
	const { type, privateKey } = params;
	try {
		if (!privateKey) throw new Error("No PrivateKey");
		return loadPrivateKeyWallet(privateKey);
	} catch (err) {
		throw err;
	}
};
